package co.simplon.alt6.firstspring.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {
    
    @RequestMapping("/")
    public List<String> list() {
        
        return List.of("Ga", "Zo", "Bu", "Meu");
    }
}
