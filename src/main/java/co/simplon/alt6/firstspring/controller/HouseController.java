package co.simplon.alt6.firstspring.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt6.firstspring.entity.House;
import co.simplon.alt6.firstspring.repository.HouseRepositoryImpl;
import co.simplon.alt6.firstspring.repository.interfaces.HouseRepository;
import jakarta.validation.Valid;

@RestController //annotation obligatoire  pour faire un contrôleur de type rest
@RequestMapping("/api/house") //permet de mettre un prefix à toutes les routes définies dans ce contrôleur
public class HouseController {
    @Autowired // Permet d'injecter un autre composant connu de spring dans cette classe (spring se charge de l'instanciation)
    private HouseRepository repo;

    @GetMapping
    public List<House> all(@RequestParam Optional<String> city) { //Ici on met le paramètre en Optional ce qui permet de gérer plus explicitement s'il possède une valeur ou non
        if(city.isPresent()) {
            return repo.findByCity(city.get());
        }
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public House one(@PathVariable int id) {
        House house = repo.find(id);
        if(house == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return house;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public House add(@Valid @RequestBody House house) {
        repo.persist(house);
        return house;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    @PatchMapping("/{id}")
    public House update(@PathVariable int id, @RequestBody House house) {
        House toUpdate = one(id);
        if(house.getPrice() != null) {
            toUpdate.setPrice(house.getPrice());
        }
        if(house.getType() != null) {
            toUpdate.setType(house.getType());
        }

        if(house.getSize() != null) {
            toUpdate.setSize(house.getSize());
        }

        if(house.getCity() != null) {
            toUpdate.setCity(house.getCity());
        }

        if(house.getAvailability() != null) {
            toUpdate.setAvailability(house.getAvailability());
        }
        
        repo.update(toUpdate);
        return toUpdate;
    }
}
