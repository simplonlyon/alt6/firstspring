package co.simplon.alt6.firstspring.entity;

import java.time.LocalDate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class House {
    private Integer id;
    @NotNull
    private Integer size;
    private String type;
    private LocalDate availability;
    private Integer price;
    @NotBlank
    private String city;
    
    public House(Integer size, String type, LocalDate availability, Integer price, String city) {
        this.size = size;
        this.type = type;
        this.availability = availability;
        this.price = price;
        this.city = city;
    }

    public House(Integer id, Integer size, String type, LocalDate availability, Integer price, String city) {
        this.id = id;
        this.size = size;
        this.type = type;
        this.availability = availability;
        this.price = price;
        this.city = city;
    }
    public House() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getSize() {
        return size;
    }
    public void setSize(Integer size) {
        this.size = size;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public LocalDate getAvailability() {
        return availability;
    }
    public void setAvailability(LocalDate availability) {
        this.availability = availability;
    }
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
}
