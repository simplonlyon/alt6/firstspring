package co.simplon.alt6.firstspring.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.firstspring.entity.House;
import co.simplon.alt6.firstspring.repository.interfaces.HouseRepository;

@Repository
public class HouseRepositoryImpl implements HouseRepository {
    @Autowired
    private DataSource dataSource;

    @Override
    public List<House> findAll() {
        List<House> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM house");
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                list.add(sqlToHouse(rs));
            }

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return list;
    }

    private House sqlToHouse(ResultSet rs) throws SQLException {
        return new House(
            rs.getInt("id"),
            rs.getInt("size"),
            rs.getString("type"),
            rs.getDate("availability").toLocalDate(), 
            rs.getInt("price"), 
            rs.getString("city"));
    }

    @Override
    public List<House> findByCity(String city) {
         List<House> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM house WHERE city=?");
            stmt.setString(1, city);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                list.add(sqlToHouse(rs));
            }

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public House find(Integer id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM house WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                return sqlToHouse(rs);
            }

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean persist(House house) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO house (size,type,price,city,availability) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

            stmt.setInt(1,house.getSize());
            stmt.setString(2, house.getType());
            stmt.setInt(3, house.getPrice());
            stmt.setString(4, house.getCity());
            stmt.setDate(5, house.getAvailability() != null ? Date.valueOf(house.getAvailability()):null);

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            house.setId(rs.getInt(1));

            return true;
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(House house) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE house SET size=?,type=?,price=?,city=?,availability=? WHERE id=?");

            stmt.setInt(1,house.getSize());
            stmt.setString(2, house.getType());
            stmt.setInt(3, house.getPrice());
            stmt.setString(4, house.getCity());
            stmt.setDate(5, house.getAvailability() != null ? Date.valueOf(house.getAvailability()):null);
            stmt.setInt(6, house.getId());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM house WHERE id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return false;
    }
}
