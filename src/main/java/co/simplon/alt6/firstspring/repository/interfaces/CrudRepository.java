package co.simplon.alt6.firstspring.repository.interfaces;

import java.util.List;


public interface CrudRepository<T> {
    List<T> findAll();
    T find(Integer id);
    boolean persist(T entity);
    boolean update(T entity);
    boolean delete(Integer id);
    
}
