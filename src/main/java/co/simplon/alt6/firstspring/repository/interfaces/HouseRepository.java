package co.simplon.alt6.firstspring.repository.interfaces;

import java.util.List;

import co.simplon.alt6.firstspring.entity.House;

public interface HouseRepository extends CrudRepository<House>{
    List<House> findByCity(String city);    
}
