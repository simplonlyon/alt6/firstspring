DROP TABLE IF EXISTS house;

CREATE TABLE house(
    id INT PRIMARY KEY AUTO_INCREMENT,
    size INT,
    type VARCHAR(255),
    price INT,
    city VARCHAR(255),
    availability DATE
);