package co.simplon.alt6.firstspring.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

/**
 * L'annotation SpringBootTest permet de lancer une application spring boot de test avec ses dépendances, ses autowires et autres.
 * L'application tourne comme "la vraie" sans autre configuration supplémentaire. On peut modifier les valeurs du application.properties
 * en en créant un autre dans le dossier test/resources qui prendra le pas sur le principal (pour utiliser une bdd de test par exemple)
 * 
 * Le AutoConfigure va permettre de faire des requêtes vers les routes définies par les contrôleurs chargées dans l'application spring
 */
@SpringBootTest
@AutoConfigureMockMvc
public class FirstControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testList() throws Exception {
        /**
         * Ici on fait un test fonctionnel où on simule directement une requête http vers l'API, on se place donc du même point de vue
         * qu'un client qui ferait une requête, sans savoir quel composant existent en arrière plan ou autre. On indique vers quel route
         * faire la requête et quelle type de requête puis on test la réponse, si le status est celui attendu et si le contenu du body
         * est celui qu'on souhaite
         */
        mvc.perform(get("/"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(4)));
    }
}
