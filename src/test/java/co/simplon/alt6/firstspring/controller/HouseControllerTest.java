package co.simplon.alt6.firstspring.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 * A noter que par défaut l'application spring ne se lance qu'une fois pour tous les tests, ce qui implique que l'exécution des fichiers
 * de la base de données ne se feront qu'une fois au début des tests, si un test supprime un id, on ne pourra peut être pas y avoir accès depuis
 * un autre test (on peut configurer l'environnement de test pour qu'il se relance entre chaque tests mais les performances s'en trouverait affectées,
 * on peut aussi faire en sorte de relancer les fichiers sql entre chaque test spécifiquement avec une annotation @SQL)
 */
@SpringBootTest
@AutoConfigureMockMvc
public class HouseControllerTest {
    @Autowired
    MockMvc mvc;

    @Test
    void testAdd() throws Exception {
        mvc.perform(post("/api/house")
        .contentType(MediaType.APPLICATION_JSON) //On fait une requête post en envoyant du JSON et en créant celui ci en dur dans le test (comme on ferait avec postman ou thunder client)
        .content("""
                {
                    "size": 20,
                    "city":"test",
                    "price":400,
                    "type":"flat",
                    "availability":"2023-01-01"
                }
                """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").isNumber()); //Ici $ représente l'objet racine JSON renvoyé par l'API, et on vérifie si l'id existe bien en number dessus
    }

    @Test
    void testAddValidationError() throws Exception {
        mvc.perform(post("/api/house")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
                {
                    
                    "city":""
                }
                """))
        .andExpect(status().isBadRequest());
    }

    @Test
    void testAll() throws Exception {
        mvc.perform(get("/api/house"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$[?(@.size == null)]").isEmpty()) //Ici on vérifie si on a bien aucun résultat dont le champ size ou city serait null
        .andExpect(jsonPath("$[?(@.city == null)]").isEmpty());
    }


    @Test
    void testCity() throws Exception {
        mvc.perform(get("/api/house?city=Le Mans"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$[?(@.size == null)]").isEmpty())
        .andExpect(jsonPath("$..city").value("Le Mans"));
    }

    @Test
    void testCityEmpty() throws Exception {
        mvc.perform(get("/api/house?city=trouville"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$", hasSize(0)));
    }


    @Test
    void testById() throws Exception {
        mvc.perform(get("/api/house/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasKey("size")))
        .andExpect(jsonPath("$.id").value(1));
    }
    @Test
    void testByIdNotFound() throws Exception {
        mvc.perform(get("/api/house/100"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testDelete() throws Exception {
        mvc.perform(delete("/api/house/3"))
        .andExpect(status().isNoContent());
    }
    @Test
    void testDeleteNotFound() throws Exception {
        mvc.perform(delete("/api/house/100000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testUpdate() throws Exception {
        mvc.perform(patch("/api/house/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
                {
                    "price":4000,
                    "type":"house"
                }
                """))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.price").value(4000))
        .andExpect(jsonPath("$.type").value("house"))
        .andExpect(jsonPath("$.city").value("Villeurbanne"))
        .andExpect(jsonPath("$.size").value(34));
    }
}
